# wallpapers

## about

This is the wallpaper collection I use on my workstation.
I back them up here and you are free to use them yourself.

## style

This repository includes **nature and landscape** photos in the wallpapers directory as well as **anime** wallpapers inside the anime directory.

## ownership

Most of the nature and landscape photos I took from [DistroTube](https://gitlab.com/dwt1/wallpapers).
The anime wallpapers are sourced from [WallpaperCave](https://wallpapercave.com/).

If any of these wallpapers are yours and you do not want them to be shared on here, contact me and I will remove them.
